<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//route CRUD
Route::get('/',[MahasiswaController:: class, 'index']);
Route::get('/tambah',[MahasiswaController:: class, 'tambah']);
Route::post('/store',[MahasiswaController:: class, 'store']);
Route::get('/edit/{id}',[MahasiswaController:: class, 'edit']);
Route::post('/update',[MahasiswaController:: class, 'update']);
Route::get('/hapus/{id}',[MahasiswaController:: class, 'hapus']);